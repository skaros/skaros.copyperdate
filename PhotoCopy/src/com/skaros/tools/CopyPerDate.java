package com.skaros.tools;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;


import static java.nio.file.StandardCopyOption.*;

public class CopyPerDate {
 File SOURCE=null;
 File DEST=null;

	public static void main(String[] args){
		args=new String[]{
//				"/run/user/1000/gvfs/mtp:host=%5Busb%3A001%2C008%5D/external storage/DCIM/Camera","/mnt/4C1EAB314334DA2B/My Photos/Baby/temp/Lenovo"};
//				"/run/user/1000/gvfs/gphoto2:host=%5Busb%3A001%2C008%5D/store_00020001/Pictures/Camera Roll/","/mnt/4C1EAB314334DA2B/My Photos/Baby/temp/All","-r"};
//				"/run/user/1000/gvfs/gphoto2:host=%5Busb%3A001%2C006%5D/DCIM/101CANON/","/mnt/4C1EAB314334DA2B/My Photos/Baby/temp/All"};
				"/mnt/4C1EAB314334DA2B/My Photos/Baby/temp/All","-r"};
		
	if(args.length<2){
			System.out.println("usage\n\tCopyPerDate [source] [destination] [Options]" +
					"\nOptions:\t-r:Rename copied files using the directory name" +
					"\n\t\t-k:Keep the old file names and use the folder name as well" +
					"");
			System.exit(1);
		}
		
		CopyPerDate cpd=new CopyPerDate();
		cpd.SOURCE=new File(args[0]);
		cpd.DEST =new File(args[1]);
		if(args.length==2){
			
			if(args[1].contains("-r")){
				List<String> folder=new ArrayList<String>();
				folder.add(args[0]);
				//one directory (source & dest one file, no coping
				cpd.rename(folder,true);
			}else{
				cpd.copy(cpd.getFiles(cpd.SOURCE));
			}
		}else{
			
			//there are options
			String options="";
			//combine all options to one string
			for(int i=2;i<args.length;i++){
				options+=args[i];
			}
			//
			if(options.contains("-r")||options.contains("-R")){		
				cpd.rename(cpd.copy(cpd.getFiles(cpd.SOURCE)),true);
			}else if(options.contains("-k")||options.contains("-K")){
				cpd.rename(cpd.copy(cpd.getFiles(cpd.SOURCE)),false);
			}
		}
		
		
	}
	
	/**
	 * gets the creation date of the image file, otherwise the lastmodified date
	 * for any other file type
	 * @param jpegFile the File 
	 * @return Date the creation date/modification date
	 * @throws IOException
	 */
	public static Date getCreationDate(File jpegFile) throws  IOException{
		Metadata metadata;
		try {
			metadata = ImageMetadataReader.readMetadata(jpegFile);
			System.out.println(jpegFile);
			ExifSubIFDDirectory directory2
	    = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
		Date date
	    = directory2.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
		
			return 	date;
		} catch (ImageProcessingException e) {
			
			//This file is not a picture
			Date date=new Date(jpegFile.lastModified());
			
				return 	date;
		}catch (NullPointerException e) {
			
			//This file is not a picture
			Date date=new Date(jpegFile.lastModified());
			
				return 	date;
		}
		
		
	}
	
	/**
	 * for the list of Strings that represent the folders, will see into every folder and rename 
	 * every folder and file in there. 
	 * !ATTENTION! this will override any older files already in these folders, not only the new ones
	 * If replace parameter is set to true, the old name will be replaced by the parent folder name
	 * with a number. If set to false the old name will be appended after the parent folder's name
	 * @param folders a List of Strings with the folders
	 * @param replace boolean, if we want to keep the old names on the files
	 */
	public void rename(List<String> folders,boolean replace){
		
		for(String folderName:folders){		
			System.out.println("££"+folderName);
			File folder=new File(folderName);
//			for(int fileNumber=1;fileNumber<folder.listFiles().length;fileNumber++){
//				
//			}
			int totalFiles=folder.list().length;
			String temp=Integer.toString(Integer.toString(totalFiles).length());
			File[] files=folder.listFiles();
			
		//	for(int fileNumber=0;fileNumber<folder.listFiles().length;fileNumber++){
			for(int fileNumber=0;fileNumber<files.length;fileNumber++){
				if(replace){
					//replace the old file name
					//find how many zeros we need
					String zeros=	String.format("%0"+temp+"d", fileNumber+1);
							
					//get the extension from the original file
					String extension = "";
					int i = files[fileNumber].getAbsolutePath().lastIndexOf('.');
					int p = files[fileNumber].getAbsolutePath().lastIndexOf(File.separator);
		
					if (i > p) {
						extension = files[fileNumber].getAbsolutePath().substring(i+1);
					}
					//use the extension and the new file name
//					destFile=
					File dest=	new File(files[fileNumber].getParentFile().getPath()+//the path to the dir
							File.separator+files[fileNumber].getParentFile().getName()+//the folders name
							"_"+zeros//insert appropriate zeros
							+"."+extension);//the extension
					System.out.println("£"+folder.listFiles()[fileNumber].getName());
					System.out.println("%"+dest);
					files[fileNumber].renameTo(dest);
				}else{
					//keep the old name and add the folder name at front
					//get the extension from the original file
				
					
					String newFilePath=files[fileNumber].getParentFile().getPath()+//the path to the dir
							File.separator+files[fileNumber].getParentFile().getName()+//the folders name
							"_"+files[fileNumber].getName();
							
					File dest=	new File(newFilePath);
					files[fileNumber].renameTo(dest);
				}
			}
			
		}
	}
	
	public void rename(){
		
	}
	
	/**
	 * copy the files from the supplied array to the coresponding folders, depending on the file's
	 * creation/modification date
	 * @param files an Array of files
	 * @return The unique folder path that were used
	 */
	public List<String> copy(File[] files) {
		List<String> destFolders=new ArrayList<String>();
		
		try {
			for (File f:files){
			
			Path src=f.toPath();

			java.util.Date date = null;
//			java.util.Date date=new Date( f.lastModified());//--mono gia fotos:
			try {
			date=	getCreationDate(f);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}//new Date( f.lastModified());
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int month = cal.get(Calendar.MONTH)+1;
			String monthS;
			if(month<10){
				monthS="0"+Integer.toString(month);
			}else
				monthS=Integer.toString(month);
			int year=cal.get(Calendar.YEAR);
			int day=cal.get(Calendar.DATE);
			String dayS;
			if(day<10){
				dayS="0"+Integer.toString(day);
			}else 
				dayS=Integer.toString(day);
			String savePath=DEST.getAbsoluteFile()+File.separator+year+File.separator+monthS+File.separator+year+"."+monthS+"."+dayS;
		
			File destFile=new File(savePath+File.separator+f.getName());
		
			if(!destFolders.contains(savePath)){
				destFolders.add(savePath);
			}

			destFile.mkdirs();			
			Path des= destFile.toPath();
			
			Files.copy(src,des, REPLACE_EXISTING);

			
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return destFolders;
		
	}
	
	/**
	 * get the files inside the folder provided and returns an array with the files
	 * sorted in ascending order
	 * @param folder
	 * @return
	 */
	public File[] getFiles(File folder){
	System.out.println(folder.getAbsolutePath());
	System.out.println(folder.exists());
		File[] filesAr = folder.listFiles();
		Arrays.sort( filesAr, new Comparator<Object>()
		{
			public int compare(Object o1, Object o2) {
				return new Long(((File)o1).lastModified()).compareTo(new Long(((File) o2).lastModified()));
			}
		});
		return filesAr;
	}
}
